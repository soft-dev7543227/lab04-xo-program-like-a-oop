/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab04_xoprogrambyoop;

/**
 *
 * @author informatics
 */
public class Table {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player player1, player2, currentPlayer;
    private int row, col;
    private int count;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean setRowCol(int row, int col) {
        if (table[row - 1][col - 1] == '-') {
            table[row - 1][col - 1] = currentPlayer.getSymbol();
            this.row = row;
            this.col = col;
            this.count++;
            return true;
        }
        return false;
    }

    public boolean checkWin() {
        if (checkRow()) {
            saveWin();
            return true;
        }
        if (checkCol()) {
            saveWin();
            return true;
        }
        if (checkDiagonal()) {
            saveWin();
            return true;
        }
        return false;
    }

    public boolean checkDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }

        player1.draw();
        player2.draw();

        return true;
    }

    private boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col - 1] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkDiagonal() {
        // check L to R
        if (table[0][0] == currentPlayer.getSymbol() && table[1][1] == currentPlayer.getSymbol() && table[2][2] == currentPlayer.getSymbol()) {
            return true;
        }

        // R to L
        if (table[0][2] == currentPlayer.getSymbol() && table[1][1] == currentPlayer.getSymbol() && table[2][0] == currentPlayer.getSymbol()) {
            return true;
        }

        return false;
    }

    private void saveWin() {
        if (currentPlayer == player1) {
            player1.win();
            player2.lose();
        } else {
            player2.win();
            player1.lose();
        }
    }

    void swicthPlayer() {
        if (currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }
}
